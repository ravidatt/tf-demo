output "subnet" {
  description = "The ID of the VPC"
  value       = aws_subnet.main_subnet.id
}
