variable "cidr_subnet" {
  type  = string


}

variable "vpc_id" {
  type  = string

}

variable "subnet_tag" {
  type  = string
  default = "tf-subnet1"

}
