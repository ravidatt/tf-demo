variable "vpc_name" {
  description = "Name to be used for VPC"
  type        = string
  default = "tf_vpc1"

}

variable "vpc_cidr_block" {
  description = "CIDR to be used for vpc"
  type        = string


}