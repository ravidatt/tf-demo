resource "aws_subnet" "main_subnet" {
  vpc_id     = var.vpc_id
  cidr_block = var.cidr_subnet

  tags = {
    Name = var.subnet_tag
  }
}